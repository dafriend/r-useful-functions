#given a cutoff point (0,1), a vector of probabilities, and the actual data, 
#converts the probabilities into predictions. Note that the actual data is only
#used to retrieve the levels. There definitely could be a better and more sleek
#way of doing it. But this works.

#=====PARAMETERS:=====

#cutoff: the smallest probability at which to predict a 1 (positive). Value should
      # be between 0 and 1 (values below zero are equivalent to setting the cutoff to 
      # 0 and values above one are equivalent to setting the cutoff to one)

#pred.raw: A vector of numbers which represent the probability of each observation
      # being a 1 (positive)

#actual: A vector containing the actual data. This is only used to retrieve the levels, 
      # so the actual content of the data is irrelevant, so long as it contains both
      # levels.

prob.predict <- function(cutoff, pred.raw, actual){
   levels <- levels(as.factor(actual))
   #convert the predicted probabilities into the binary response
   pred <- ifelse(pred.raw < cutoff, levels[1], levels[2])
   #if none of the predictions fall into one of the two categories, we need to 
   #make sure that both levels are included in the factor (otherwise the table() 
   #function will leave out a row)
   pred <- factor(pred, levels=levels)
   return(pred)
}