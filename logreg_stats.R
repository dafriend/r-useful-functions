#function for calculating stats for predictions from logistic regression models

#=====PARAMETERS:=====

#cutoff -> lowest percentage at which a 1 is predicted

#pred.raw -> predictions in the form of probabilities

#actual -> the actual reponse variable, as a factor with two levels

logreg.stats <- function(cutoff, pred.raw, actual){
   #get the levels of the originial response values - the assumption is that there's only 
   #two levels (and no less than two)
   levels <- levels(as.factor(actual))
   #convert the predicted probabilities into the binary response
   pred <- ifelse(pred.raw < cutoff, levels[1], levels[2])
   #if none of the predictions fall into one of the two categories, we need to make sure 
   #that both levels are included in the factor (otherwise the table() function will leave out a row)
   pred <- factor(pred, levels=levels)
   #make a contingency table 
   cont.table <- table(pred, actual, dnn=c("Predicted", "Actual"))
   #calculate the stats
   misclass <- (1 - (sum(diag(cont.table))/sum(cont.table)))*100
   sensitivity <- (cont.table[2,2]/sum(cont.table[,2]))*100
   specificity <- (cont.table[1,1]/sum(cont.table[,1]))*100
   
   #make a table that contains all of the stats
   results <- round(rbind(misclass, sensitivity, specificity),digits=2)
   rownames(results) <- c("Misclassification Rate", "Sensitivity", "Specificity")
   colnames(results) <- c("%")
   
   #return the results as a list
   list.return <- list("table" = cont.table, "stats.all" = results, "misclassification" = misclass, "sensitivity" = sensitivity, "specificity" = specificity)
   return(list.return)
}