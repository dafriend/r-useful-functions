
library(vcd)

#same function as before, but with the default plots
plot.column.default <- function(df, col.name, factor.cutoff = 30){
   df.classes = sapply(df, class)
   df.integer.indices = df.classes == "integer"
   if(sum(df.integer.indices) > 0){
      warning(paste0(sum(df.integer.indices), " columns of class 'integer' were found. Converting to 'numeric'."))
      df[,df.integer.indices] = sapply(df[,df.integer.indices], as.numeric)
   }

   #get the index of the column
   col.index <- which(names(df)==col.name)
   #save the data in a variable (mostly for readability)
   col.data <- df[,col.index]

   #create an index variable - we won't use i for the index because it's possible we'll skip columns (if they're not the right class) which would put NULLs in our list
   index <- 1
   #only go on if the specified column is of class 'factor' or 'numeric'
   if(class(df[,col.index]) == "factor" || class(df[,col.index]) == "numeric"){
      #loop through each column in the data set
      for(i in 1:ncol(df)){
         #we don't want to plot the column against itself
         if(i != col.index){

            #get the current column and column name. This is mostly for readability.
            i.name <- colnames(df)[i]
            i.data <- df[,i]

            #create a data frame with the two columns. We'll use this for ggplot.
            temp.df <- data.frame(col = col.data, i = i.data, stringsAsFactors = FALSE)

            #now go through the four possible combination of class types
            if(class(temp.df$col) == "factor" && class(temp.df$i) == "factor"){
               #if there's too many factors, don't plot, because it will look terrible and be useless

               if(length(levels(temp.df$i)) <= factor.cutoff){
                  #We'll do a barplot of the ith column, grouped by our selected column
                  cont.table <- table(temp.df$col, temp.df$i)
                  mosaicplot(cont.table, main=i.name, xlab=col.name, ylab=i.name)
                  #ggplots[[index]] <- ggplot(temp.df, aes(x=i)) +
                  #   geom_bar() +
                  #   facet_wrap(~col) +
                  #   ggtitle(i.name) +
                  #   xlab(i.name)
                  #increment the index
                  index <- index + 1
               } else {
                  #warn the user that a column was not plotted
                  warning(paste0("The column '", i.name, "' has more than ", factor.cutoff, " levels and was not plotted.\n"))
               }
            }
            else if(class(temp.df$col) == "factor" && class(temp.df$i) == "numeric"){
               #we'll make a boxplot with the ith column on y and grouped by our selected column along the x axis
               boxplot(i ~ col, data=temp.df, xlab=col.name, ylab=i.name, main=i.name)
               #ggplots[[index]] <- ggplot(temp.df, aes(x=col, y=i)) +
               #   geom_boxplot() +
               #   ggtitle(i.name) +
               #   xlab(col.name) +
               #   ylab(i.name)
               #increment the index
               index <- index + 1

            }
            else if(class(temp.df$col) == "numeric" && class(temp.df$i) == "factor")
            {
               #again, don't plot if there's too many factors
               if(length(levels(temp.df$i)) <= factor.cutoff){
                  #we'll make a boxplot with our selected column on y and grouped by the ith column along the x axis
                  boxplot(col ~ i, data=temp.df, xlab=i.name, ylab=col.name, main=i.name)
                  #ggplots[[index]] <- ggplot(temp.df, aes(x=i, y=col)) +
                  #   geom_boxplot() +
                  #   ggtitle(i.name) +
                  #   xlab(i.name) +
                  #   ylab(col.name)
                  #increment the index
                  index <- index + 1
               } else {
                  #warn the user that a column was not plotted
                  warning(paste0("The column '", i.name, "' has more than ", factor.cutoff, " factors and was not plotted.\n"))
               }

            }
            else if(class(temp.df$col) == "numeric" && class(temp.df$i) == "numeric"){
               #we'll make a scatter plot with our selected column on y and the ith column on x
               plot(col ~ i, data=temp.df, xlab=i.name, ylab=col.name, main=i.name)
               #ggplots[[index]] <- ggplot(temp.df, aes(x=i, y=col)) +
               #   geom_point() +
               #   ggtitle(i.name) +
               #   xlab(i.name) +
               #   ylab(col.name)
               #increment the index
               index <- index + 1

            }
            else {
               #warn the user that a column wasn't plotted
               warning(paste0("The column '", i.name, "' is of class '", class(temp.df$i), "' and was not plotted. Data must be of class 'factor' or 'numeric'.\n"))
            }
         }
      }
   } else {
      #warn the user that the selected column was of the wrong class
      warning(paste0("The column '", col.name, "' is of class '", class(df[,col.index]),"' and was not plotted. Data must be of class 'factor' or 'numeric'.\n"))
      return()
   }
}
