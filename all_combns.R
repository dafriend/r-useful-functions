#given a vector, this function returns all possible combinations (of all lengths) of the elements of the vector
all.combns <- function(data){
   combns <-list()
   counter <- 1
   for (i in 1:length(data)){
      temp <- combn(data, i)
      for (j in 1:ncol(temp)){
         combns[[counter]] <- as.vector(temp[,j])
         counter <- counter + 1
      }
   }
   return(combns)
}