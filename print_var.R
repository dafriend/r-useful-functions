#debugging function for printing a variable's name and it's value

#thing: an object
print_var = function(thing){
   var_name = deparse(substitute(thing))
   print(paste0(var_name, ": ", thing))
}